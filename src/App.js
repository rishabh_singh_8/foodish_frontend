import "./App.css";
import AddReview from "./Pages/AddReview/AddReview";
import Navbar from "./Components/Navbar/Navbar";
import AllReviews from "./Pages/AllReviews/AllReviews";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import Login from "./Pages/Login/Login";

function App() {
  return (
    <div className="App">
      <BrowserRouter>
        <Routes>
            <Route path="/" exact element={<Login />} />
            <Route path="/all" element={<AllReviews />} />
            <Route path="/add" element={<AddReview />} />
        </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;
