import React from "react";
import { Button } from "@mui/material";
import "./Btn.css";

const Btn = ({ title,submit,handleClick }) => {
  return (
    <div>
      <Button
        variant="outlined"
        className="btn"
        type={submit}
        style={{ borderColor: "grey", color: "black",marginTop:".3rem" }}
        onClick={handleClick}
      >
        {title}
      </Button>
    </div>
  );
};

export default Btn;

// import * as React from 'react';
// import Button, { ButtonProps } from "@material-ui/core/Button";
// import { Theme } from '@material-ui/core';
// import { withStyles } from '@material-ui/styles';

// const styles: (theme: Theme) => any = (theme) => {
//     return {
//         root:{
//             backgroundColor: theme.palette.error.main,
//             color: theme.palette.error.contrastText,
//             "&:hover":{
//                 backgroundColor: theme.palette.error.dark
//             },
//             "&:disabled":{
//                 backgroundColor: theme.palette.error.light
//             }
//         }
//     };
// };

// export const Btn = withStyles(styles)((props: ButtonProps) => {
//     const { className, ...rest } = props;
//     const classes = props.classes||{};
//     return <Button {...props} className={`${className} ${classes.root}`} variant="contained" />
// });
