import { Link } from 'react-router-dom'
import React from 'react'
import './Navbar.css'
import { Button } from '@mui/material'
import { useNavigate } from "react-router-dom";

const Navbar = () => {
    const navigate = useNavigate();
    const handleLogout = ()=>{
        localStorage.removeItem('token');
        navigate("/");
    }
    return (
        <div className='navi'>
            <div className="logo">
            <Link to={"/all"} className='links'><b>Foodish</b></Link>
            </div>
            <div className="pages">
                <ul>
                    <li><Link to={"/all"} className='links'>Reviews</Link></li>
                    <li> <Link to={"/add"} className='links'>Add Review</Link></li>
                    <li><Button variant="outlined" onClick={handleLogout} size="small">Logout</Button> </li> 
                </ul>
            </div>
        </div>   
    )
}

export default Navbar
