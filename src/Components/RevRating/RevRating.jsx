import React from "react";
import { Rating, Typography } from "@mui/material";
// import Comment from '../Comment/Comment';
import "./RevRating.css";

const RevRating = ({ comment,name,value,handleChange }) => {


  return (
    <div className="rating">
      <Typography className="comment">{comment}</Typography>
      <Rating
        name={name}
        value={value}
        max={10}
    onChange={handleChange}
    style={{margin:"0.3rem"}}
      />
    </div>
  );
};

export default RevRating;
