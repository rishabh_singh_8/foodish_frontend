import React from "react";
import Star from "../Star/Star";
import "./ReviewCard.css";

const ReviewCard = ({
  review,
  time,
  name,
  cuisine,
  food,
  location,
  rating,
}) => {
  return (
    <div className="card">
      <div className="row1">
        <div className="star">
          <Star star={rating} />
        </div>
        <div className="time">
          <p>{time}</p>
        </div>
      </div>
      <div className="division">
        <div className="leftPart">
          <div className="row3">
            
            <h3>{name}</h3>
            <h6>{location}</h6>
          </div>
          <div className="row2">
          <h2>{food}</h2>
            <h6>{cuisine} </h6>
          </div>
        </div>
        <div className="rightPart">
          <div className="compRev">
            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Perspiciatis debitis quia fugit obcaecati dolorem a, magnam enim sequi cupiditate corrupti ipsum dolorum ab reiciendis sint animi atque eius omnis at.
            Voluptatum eius, placeat optio magni incidunt adipisci, eaque quia alias omnis consequatur dolore quae accusantium mollitia modi iste vitae reiciendis nemo fugiat repellat soluta, minima numquam? Iusto culpa ducimus natus?
            Molestias exercitationem alias tempore! Repellendus sapiente quibusdam id corporis recusandae nulla esse cumque at officiis odit? Distinctio, eum provident mollitia accusantium incidunt voluptas officia quisquam delectus, doloremque corrupti, repellendus magni.</p>
          </div>
        </div>
      </div>
    </div>
  );
};

export default ReviewCard;
