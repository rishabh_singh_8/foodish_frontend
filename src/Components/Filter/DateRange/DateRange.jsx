import * as React from 'react';
import TextField from '@mui/material/TextField';
import AdapterDateFns from '@mui/lab/AdapterDateFns';
import LocalizationProvider from '@mui/lab/LocalizationProvider';
import DatePicker from '@mui/lab/DatePicker';


export default function ResponsiveDatePickers() {
  const [fromDate, setFrom] = React.useState(new Date());
  const [toDate, setTO] = React.useState(new Date());

  return (
    <LocalizationProvider dateAdapter={AdapterDateFns}>
        <DatePicker
          disableFuture
          label="From"
          openTo="day"
          views={['year', 'month', 'day']}
          value={fromDate}
          onChange={(newValue) => {
            setFrom(newValue);
          }}
          renderInput={(params) => <TextField {...params} />}
        />
        <DatePicker
          disableFuture
          label="To"
          openTo="day"
          views={['year', 'month', 'day']}
          value={toDate}
          onChange={(newValue) => {
            setTO(newValue);
          }}
          renderInput={(params) => <TextField {...params} />}
        />
    </LocalizationProvider>
  );
}