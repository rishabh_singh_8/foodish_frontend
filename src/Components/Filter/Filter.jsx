// import { Stack, Chip, Accordion, AccordionDetails, AccordionSummary, Typography } from '@mui/material';
// import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
// import React, { useState } from 'react';
// import SuggestText from '../SuggestText/SuggestText';
// import Btn from '../Btn/Btn';
// import './Filter.css'
// import DateRange from './DateRange/DateRange';

// const Filter = () => {

//     const restaurant = ["KFC", "McDonalds", "Burger King", "Meghna's Biryani", "CCD", "Olivera", "1912"];
//     const location = ["Bangalore", "Delhi", "Kolkata", "Chennai", "Ahemdabad"];
//     const food = ["Burger", "Sandwich", "Pizza", "Noodles", "Biryani", "Sweets", "Tandoori Chicken", "Juice"];
//     const rating = ["10", "9", "8", "7", "6", "5", "4", "3", "2", "1"];
//     const dates = ["1", "2"];

//     const [param, setParam] = useState(restaurant);
//     const [variRes, setVariRes] = useState("outlined");
//     const [variDish, setVariDish] = useState("outlined");
//     const [variLoc, setVariLoc] = useState("outlined");
//     const [variDate, setVariDate] = useState("outlined");
//     const [variRate, setVariRate] = useState("outlined");
//     const [inp, setInp] = useState("text");

//     const searchRestaurant = () => {
//         console.log("restraunt");
//         setParam(restaurant);
//         setVariRes("filled");
//         setVariDish("outlined");
//         setVariLoc("outlined");
//         setVariDate("outlined");
//         setVariRate("outlined");
//         setInp("text");
//     }
//     const searchDish = () => {
//         console.log("dish");
//         setParam(food);
//         setVariRes("outlined");
//         setVariDish("filled");
//         setVariLoc("outlined");
//         setVariDate("outlined");
//         setVariRate("outlined");
//         setInp("text");
//     }
//     const searchLocation = () => {
//         console.log("location");
//         setParam(location);
//         setVariRes("outlined");
//         setVariDish("outlined");
//         setVariLoc("filled");
//         setVariDate("outlined");
//         setVariRate("outlined");
//         setInp("text");
//     }

//     const searchDate = () => {
//         console.log("date");
//         setParam(dates);
//         setVariRes("outlined");
//         setVariDish("outlined");
//         setVariLoc("outlined");
//         setVariDate("filled");
//         setVariRate("outlined");
//         setInp("date");
//     }

//     const searchRate = () => {
//         console.log("rating");
//         setParam(rating);
//         setVariRes("outlined");
//         setVariDish("outlined");
//         setVariLoc("outlined");
//         setVariDate("outlined");
//         setVariRate("filled");
//         setInp("text");
//     }

//     return (
//             <div className='filterComp'>
//                 <h3>Filters</h3>

//                 <div className="types">
//                     <h5>Choose filter type</h5>
//                     <div className="param">
//                         <Chip label="Restaurant" variant={variRes} onClick={searchRestaurant} />
//                         <Chip label="Dish" variant={variDish} onClick={searchDish} />
//                         <Chip label="Location" variant={variLoc} onClick={searchLocation} />
//                         <Chip label="Rating" variant={variRate} onClick={searchRate} />
//                         <Chip label="Date" variant={variDate} onClick={searchDate} />

//                     </div>
//                 </div>

//                 <div className="searchFieldCont">
//                     <Stack spacing={2} className='searchField'>
//                         {(inp === "date") ?
//                             <DateRange /> : <SuggestText title={"Enter details ..."} options={param} />}
//                         <Btn title={"SEARCH"} />
//                     </Stack>
//                 </div>

//             <div className="mobilefilter">
//                 <Accordion>
//                     <AccordionSummary
//                         expandIcon={<ExpandMoreIcon />}
//                         aria-controls="panel1a-content"
//                         id="panel1a-header"
//                     >
//                         <Typography>Filters</Typography>
//                     </AccordionSummary>
//                     <AccordionDetails>
//                         <div className="mobileTypes">
//                             <h5>Choose filter type</h5>
//                             <div className="param">
//                                 <Chip label="Restaurant" variant={variRes} onClick={searchRestaurant} />
//                                 <Chip label="Dish" variant={variDish} onClick={searchDish} />
//                                 <Chip label="Location" variant={variLoc} onClick={searchLocation} />
//                                 <Chip label="Rating" variant={variRate} onClick={searchRate} />
//                                 <Chip label="Date" variant={variDate} onClick={searchDate} />

//                             </div>
//                         </div>

//                         <div className="mobilesearchFieldCont">
//                             <Stack spacing={2} className='searchField'>
//                                 {(inp === "date") ?
//                                     <DateRange /> : <SuggestText title={"Enter details ..."} options={param} />}
//                                 <Btn title={"SEARCH"} />
//                             </Stack>
//                         </div>
//                     </AccordionDetails>
//                 </Accordion>
//             </div>
//             </div>

//     )
// }

// export default Filter

import { Alert, TextField, Typography } from "@mui/material";
import React, { useState } from "react";
import Btn from "../Btn/Btn";
import { useFormik } from "formik";

import "./Filter.css";
import axios from "axios";


const Filter = ({ setloader, setData }) => {
  const [alert, setAlert] = useState(false);
  const formik = useFormik({
    initialValues: {
      restaurant: "",
      food: "",
      location: "",
      min: 1,
      max: 10,

    },
    onSubmit: async (values) => {
      if (values.min > values.max) {
        setAlert(true);
        setTimeout(() => {
          setAlert(false);
        }, 3000);
      } else {
        let data = await axios.post(
          `http://localhost:12000/filter?page=1&limit=20`,
          {
            filters: [
              {
                key: "restaurant",
                op: "startsWith",
                value: values.restaurant,
              },
              {
                key: "food",
                op: "startsWith",
                value: values.food,
              },

              {
                key: "location",
                op: "startsWith",
                value: values.location,
              },
              {
                key: "rating",
                op: "between",
                value: [values.min, values.max],
              },

            ],
          }
        );
        setData(data.data.reviews);
      }
    },
  });

  return (
    <div className="container">
      <div className="b1">
        <div className="content">
          <h3>Filters</h3>

          <form onSubmit={formik.handleSubmit}>
            <TextField
              name="restaurant"
              label="Restaurant"
              value={formik.values.restaurant}
              onChange={formik.handleChange}
              style={{ margin: "0.6rem" }}
            />
            <TextField
              name="food"
              label="Food"
              value={formik.values.food}
              onChange={formik.handleChange}
              style={{ margin: "0.6rem" }}
            />
            <TextField
              name="location"
              label="Location"
              value={formik.values.location}
              onChange={formik.handleChange}
              style={{ margin: "0.6rem" }}
            />
            <TextField
              name="min"
              label="Min Rating"
              value={formik.values.min}
              onChange={formik.handleChange}
              style={{ margin: "0.6rem" }}
            />
            <TextField
              name="max"
              label="Max Rating"
              value={formik.values.max}
              onChange={formik.handleChange}
              style={{ margin: "0.6rem" }}
            />

            {/* <Slideer
              name="ratingSlider,ratingsSlider1"
              value={[formik.values.ratingSlider,formik.values.ratingSlider1]}
              handleChange={formik.handleChange}
            /> */}

            <Btn title="Search" submit="submit" />
          </form>
          {alert ? (
            <Alert severity={"warning"} onClose={()=>setAlert(false)}>
              Min rating cannot be greater than Max rating
            </Alert>
          ) : null}
        </div>
      </div>
    </div>
  );
};

export default Filter;
