import React from "react";
import { TextField, Autocomplete } from "@mui/material";
import "./SuggestText.css";

const SuggestText = ({ title, options, name, value, handleChange }) => {

  return (
    <div className="field">
      <Autocomplete
        disablePortal
        id="combo-box-demo"
        options={options}
        autoComplete
        freeSolo={true}
        sx={{ width: "100%" }}

        renderInput={(params) => (
          <TextField
            {...params}
            value={value}
            name={name}
            onChange={handleChange}
            label={title}
            fullWidth
            style={{ margin: ".3rem" }}
          />
        )}
      />
    </div>
  );
};

SuggestText.defaultProps = {
  width: "300",
  height: "50",
  multiline: false,
};

export default SuggestText;
