// import { Rating } from '@mui/material';
// import StarIcon from '@mui/icons-material/Star';
// import { styled } from '@mui/material/styles';
import React from 'react'
import StarIcon from '@mui/icons-material/Star';
import './Star.css'

// const StyledStar = styled(Rating)({
//     '& .MuiRating-iconFilled': {
//       color: 'darkgoldenrod',
//     }
//   });


const Star = ({ star }) => {
    return (
        <div className='starRating'>
           <StarIcon />  <span>{star}</span> 
        </div>
    )
}

export default Star
