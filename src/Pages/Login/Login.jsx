import { TextField } from "@mui/material";
import React, { useState } from "react";
import Btn from "../../Components/Btn/Btn";
import Alert from '@mui/material/Alert';
import "./login.css";
import axios from 'axios';
import sign from './sign.svg'
import { useFormik } from "formik";
import { useNavigate } from "react-router-dom";


const Login = () => {
  const navigate = useNavigate();
  const [alert, setAlert] = useState(false);
  const formik = useFormik({
    initialValues: {
      username: "",
      password: "",
    },
    onSubmit: async () => {
      try {
        let res = await axios.post('http://localhost:12000/login', {
          "username": formik.values.username,
          "password": formik.values.password
        });
        let token = res["data"]["token"].split(" ")[1];
        console.log(res);
        console.log(token);
        localStorage.setItem('token', token);
        navigate('/all');

      }
      catch (err) {
        setAlert(!alert);
      }
    }
  });

  return (
    <div className="login_page">
      <div className="fdsh_logo"><b>FOODISH</b></div>
    <div className="login">
      <img src={sign} alt={"signin"} className="reviewImage" />
      <div className="box">
        <h2>Login to enter</h2>
        {alert ? <Alert severity="error" onClose={() => { setAlert(false) }}>Invalid login details.</Alert> : null}
        <div className="form_login">
          <form onSubmit={formik.handleSubmit}>
            <TextField
              label="Username"
              name="username"
              value={formik.values.user}
              onChange={formik.handleChange}
              style={{ margin: "1rem" }}
            />
            <TextField
              label="Password"
              name="password"
              value={formik.values.pass}
              onChange={formik.handleChange}
              style={{ margin: "1rem", marginBottom: "2rem" }}
              type="password"
            />
            <Btn submit="submit" title="Login" style={{ margin: "23rem" }} />
          </form>
        </div>
      </div>
    </div>
    </div>
  );
};

export default Login;