import React, { useEffect, useState } from "react";
import Filter from "../../Components/Filter/Filter";
import "./AllReviews.css";
import axios from "axios";
import ReviewCard from "../../Components/ReviewCard/ReviewCard";
import Navbar from "../../Components/Navbar/Navbar"
import { Link } from "react-router-dom";

const AllReviews = () => {
  const [data, setData] = useState([]);

  const isLoggedIn = () => {
    const token = localStorage.getItem('token');
    if(token){
        return true;
    }
    return false;
}


  useEffect(() => {
    const fetchReview = async () => {
      const data = await axios.post(
        `http://localhost:12000/filter?page=1&limit=20`
      );
     setData(data.data);
    console.log(data.data);
    };
    fetchReview();
  },[]);

  return (
    <div>
      <Navbar />
    {isLoggedIn()?<div className="allReviews">
      <div className="title">All Reviews</div>
      <div className="AllReviewDivision">
        <div className="filters">
          <Filter
           setData={setData}

           />
        </div>
        <div className="reviews">
          
          {data.map((el) => (
            <ReviewCard
              key={el.id}
              name={el.restaurant}
              ambience={el.ambience}
              cuisine={el.cuisine}
              food={el.food}
              location={el.location}
              time={el.time}
              rating={el.rating}
              review={el.review}
            />
          ))}
        </div>
      </div>
    </div>:
    <h2 className='error_msg'>You are logged out! Go back to <Link to="/">Login Page</Link></h2>}
    </div>
  );
};

export default AllReviews;
