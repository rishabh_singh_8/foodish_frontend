import { Alert, TextField } from "@mui/material";
import React, { useState } from "react";
import SuggestText from "../../Components/SuggestText/SuggestText";
import Btn from "../../Components/Btn/Btn";
import "./AddReview.css";
import rev from "./rev.svg";
import axios from "axios";
import { useFormik } from "formik";
import RevRating from "../../Components/RevRating/RevRating";
import { Link } from "react-router-dom";
import Navbar from "../../Components/Navbar/Navbar";

const AddReview = () => {

  const isLoggedIn = () => {
    const token = localStorage.getItem('token');
    if (token) {
      return true;
    }
    return false;
  }

  const [alert, setAlert] = useState(false)
  const formik = useFormik({
    initialValues: {
      rname: "",
      location: "",
      dish: "",
      cuisine: "",
      food: 1,
      ambience: 1,
      hygiene: 1,
      review: "",
    },
    onSubmit: (values) => {
      axios.post(`http://localhost:12000/add`, {
        restaurant: values.rname,
        location: values.location,
        food: values.dish,
        review: values.review,
        cuisine: values.cuisine,
        ratings: [
          {
            ratingType: "Ambience",
            rating: parseInt(values.ambience),
          },
          {
            ratingType: "Food",
            rating: parseInt(values.food),
          },
          {
            ratingType: "Hygiene",
            rating: parseInt(values.hygiene),
          },
        ],
      });
      setAlert(true)
      setTimeout(() => {
        setAlert(false)
      }, 4000);
    },
  });

  const restaurant = [];
  const location = [];
  const food = [];
  const cuisine = [];
  // const fullwidth = "fullwidth";

  return (
    <div>
      <Navbar />
      {isLoggedIn() ? <div className="addReview">
        <div className="title">Add Review</div>
        {alert ? (
          <Alert severity={"success"} onClose={() => setAlert(false)}>
            Review Added successfully
          </Alert>
        ) : null}
        <div className="inputFields">
          <img src={rev} alt="Add Review" className="reviewImage" />
          {/* <Stack spacing={1.5} className="form"> */}
          <form className="form" onSubmit={formik.handleSubmit}>
            <SuggestText
              handleChange={formik.handleChange}
              value={formik.values.rname}
              title={"Restaurant Name"}
              name="rname"
              options={restaurant}
            />
            <SuggestText
              handleChange={formik.handleChange}
              value={formik.values.location}
              title={"Location"}
              name="location"
              options={location}
            />
            <SuggestText
              handleChange={formik.handleChange}
              value={formik.values.dish}
              title={"Dish"}
              name="dish"
              options={food}
            />
            <SuggestText
              handleChange={formik.handleChange}
              value={formik.values.cuisine}
              title={"Cuisine"}
              name="cuisine"
              options={cuisine}
            />
            <RevRating
              handleChange={formik.handleChange}
              value={formik.values.food}
              comment={"Food"}
              name="food"
            />
            <RevRating
              handleChange={formik.handleChange}
              value={formik.values.hygiene}
              comment={"Hygiene"}
              name="hygiene"
            />
            <RevRating
              handleChange={formik.handleChange}
              value={formik.values.ambience}
              comment={"Ambience"}
              name="ambience"
            />
            <TextField
              name="review"
              value={formik.values.review}
              onChange={formik.handleChange}
              label="Review"
              multiline={true}
              rows={3}
              fullWidth
            />

            <Btn submit="submit" title={"SUBMIT REVIEW"} />
          </form>
          {/* </Stack> */}
        </div>
      </div>:
          <h2 className="error_msg">You are logged out! Go back to <Link to="/">Login Page</Link></h2>}
    </div>
  );
};

export default AddReview;
